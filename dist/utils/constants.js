"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapStatName = void 0;
exports.MapStatName = {
    attack: 'ATTACK',
    defense: 'DEFENSE',
    hp: 'HP',
    'special-attack': 'SPECIAL_ATTACK',
    'special-defense': 'SPECIAL_DEFENSE',
    speed: 'SPEED'
};
//# sourceMappingURL=constants.js.map