"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pokemon = void 0;
exports.Pokemon = ({ prisma, uid, pokemonId, name }) => {
    const getPokemonByUser = () => __awaiter(void 0, void 0, void 0, function* () { return yield prisma.pokemon.findFirst({ where: { pokemonId, userId: uid } }); });
    const createPokemon = () => __awaiter(void 0, void 0, void 0, function* () { return yield prisma.pokemon.create({ data: { name, pokemonId, user: { connect: { id: uid } } } }); });
    const getAllPokemonsByUser = () => __awaiter(void 0, void 0, void 0, function* () { return yield prisma.pokemon.findMany({ where: { userId: uid } }); });
    const removePokemonByUser = ({ id }) => __awaiter(void 0, void 0, void 0, function* () { return yield prisma.pokemon.delete({ where: { id } }); });
    return {
        getPokemonByUser,
        createPokemon,
        getAllPokemonsByUser,
        removePokemonByUser
    };
};
//# sourceMappingURL=Pokemon.js.map