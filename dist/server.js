"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_1 = require("apollo-server");
const context_1 = require("./graphql/context");
const pokemonAPI_1 = require("./graphql/datasources/pokemonAPI");
const schema_1 = require("./graphql/schema");
const server = new apollo_server_1.ApolloServer(Object.assign(Object.assign({}, schema_1.default), { context: context_1.createContext, dataSources: () => ({
        pokemonAPI: new pokemonAPI_1.default()
    }) }));
server.listen({ port: 5001 }).then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
});
//# sourceMappingURL=server.js.map