"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("@utils/constants");
const apollo_datasource_rest_1 = require("apollo-datasource-rest");
class PokemonAPI extends apollo_datasource_rest_1.RESTDataSource {
    constructor() {
        super();
        this.baseURL = "https://pokeapi.co/api/v2/";
    }
    listPokemons(offset) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.get(`pokemon?limit=50&offset=${offset}`);
            }
            catch (error) {
                throw new Error('Could not get pokemons: ' + error);
            }
        });
    }
    getPokemonDetail(name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const _a = yield this.get(`pokemon/${name}`), { sprites: { front_default }, stats } = _a, rest = __rest(_a, ["sprites", "stats"]);
                const pokemonStats = stats.map(stat => ({ name: constants_1.MapStatName[stat.stat.name], value: stat.base_stat }));
                return Object.assign(Object.assign({}, rest), { stats: pokemonStats, image: front_default });
            }
            catch (error) {
                throw new Error('Could not get pokemon detail ' + error);
            }
        });
    }
}
exports.default = PokemonAPI;
//# sourceMappingURL=index.js.map