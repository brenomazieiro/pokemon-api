"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createContext = void 0;
const client_1 = require("@prisma/client");
const auth_1 = require("@services/firebase/auth");
const apollo_server_1 = require("apollo-server");
const prisma = new client_1.PrismaClient();
exports.createContext = ({ req }) => __awaiter(void 0, void 0, void 0, function* () {
    const authorization = req.headers.authorization;
    if (!authorization)
        throw new apollo_server_1.AuthenticationError("User not autheticated");
    const idToken = authorization.split(" ")[1];
    const validatedIdToken = yield auth_1.validateIdToken(idToken);
    if (!validatedIdToken.uid)
        throw new apollo_server_1.AuthenticationError("Token expired");
    return {
        uid: validatedIdToken.uid,
        prisma
    };
});
//# sourceMappingURL=context.js.map