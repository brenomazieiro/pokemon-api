"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("graphql-import-node");
const resolvers_1 = require("./resolvers");
const typeDefs = require("./schema.graphql");
const schema = {
    typeDefs,
    resolvers: resolvers_1.default
};
exports.default = schema;
//# sourceMappingURL=schema.js.map