"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Mutation_1 = require("./Mutation");
const Pokemon_1 = require("./Pokemon");
const Query_1 = require("./Query");
const User_1 = require("./User");
const resolvers = {
    Query: Query_1.default,
    Mutation: Mutation_1.default,
    Pokemon: Pokemon_1.default,
    User: User_1.default
};
exports.default = resolvers;
//# sourceMappingURL=index.js.map