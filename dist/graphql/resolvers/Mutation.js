"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("@models/index");
const Mutation = {
    createUser: (_, { user }, { uid, prisma }) => __awaiter(void 0, void 0, void 0, function* () {
        const userModel = index_1.User(Object.assign({ uid, prisma }, user));
        return yield userModel.createUser();
    }),
    savePokemon: (_, { pokemon }, { uid, prisma }) => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonModel = index_1.Pokemon(Object.assign({ uid, prisma }, pokemon));
        const wasCreated = yield pokemonModel.createPokemon();
        return !!wasCreated;
    }),
    removePokemon: (_, { pokemonId }, { uid, prisma }) => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonModel = yield index_1.Pokemon({ uid, prisma, pokemonId });
        const pokemon = yield pokemonModel.getPokemonByUser();
        if (!pokemon)
            return false;
        const wasDeleted = yield pokemonModel.removePokemonByUser({ id: pokemon.id });
        return !!wasDeleted;
    })
};
exports.default = Mutation;
//# sourceMappingURL=Mutation.js.map