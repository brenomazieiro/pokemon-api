"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("@models/index");
const Query = {
    pokemons: (_, { offset = 0 }, { dataSources }) => __awaiter(void 0, void 0, void 0, function* () {
        const { results } = yield dataSources.pokemonAPI.listPokemons(offset);
        const pokemons = yield Promise.all(results.map(pokemon => dataSources.pokemonAPI.getPokemonDetail(pokemon.name)));
        return pokemons;
    }),
    user: (_, __, { uid, prisma }) => __awaiter(void 0, void 0, void 0, function* () {
        const userModel = index_1.User({ uid, prisma });
        return yield userModel.getUser();
    })
};
exports.default = Query;
//# sourceMappingURL=Query.js.map