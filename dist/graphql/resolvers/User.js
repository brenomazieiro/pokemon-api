"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Pokemon_1 = require("@models/Pokemon");
const User = {
    pokemons: (_, __, { uid, prisma, dataSources }) => __awaiter(void 0, void 0, void 0, function* () {
        const pokemonModel = Pokemon_1.Pokemon({ uid, prisma });
        const pokemonList = yield pokemonModel.getAllPokemonsByUser();
        const pokemons = yield Promise.all(pokemonList.map(pokemon => dataSources.pokemonAPI.getPokemonDetail(pokemon.name)));
        return pokemons;
    })
};
exports.default = User;
//# sourceMappingURL=User.js.map