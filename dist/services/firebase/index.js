"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const app = admin.initializeApp({ credential: admin.credential.applicationDefault() });
exports.default = app;
//# sourceMappingURL=index.js.map