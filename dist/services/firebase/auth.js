"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateIdToken = void 0;
const index_1 = require("./index");
exports.validateIdToken = (idToken) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield index_1.default.auth().verifyIdToken(idToken).then(decodedToken => ({ uid: decodedToken.uid }));
    }
    catch (_a) {
        return { uid: undefined };
    }
});
//# sourceMappingURL=auth.js.map