import { ApolloServer } from "apollo-server";
import { createContext } from "./graphql/context";
import PokemonAPI from "./graphql/datasources/pokemonAPI";
import schema from "./graphql/schema";

const server = new ApolloServer({
  ...schema,
  context: createContext,
  dataSources: () => ({
    pokemonAPI: new PokemonAPI()
  }),
});

server.listen({ port: 5001 }).then(({ url }: { url: string }) => {
  console.log(`🚀  Server ready at ${url}`);
})