import { PrismaClient } from "@prisma/client"
import { StatName as StatNameGenerated } from "../generated/graphql"
import PokemonAPI from "../graphql/datasources/pokemonAPI"

type Results = {
  name: string,
  url: string
}

export type PokemonListResponse = {
  next: string,
  previous: string,
  results: Results[]
}

type Ability = {
  name: string
}

type Stat = {
  base_stat: number,
  stat: {
    name: string
  }
}

type Type = {
  type: {
    name: string
  }
}

export type PokemonResponse = {
  id: number,
  name: string,
  height: number,
  weight: number,
  sprites: {
    front_default: string
  },
  abilities: Ability[],
  stats: Stat[],
  types: Type[]
}

export interface DataSources {
  pokemonAPI: PokemonAPI;
}

export type Context = {
  dataSources: DataSources;
  uid?: string,
  prisma: PrismaClient
};

export type StatName = Record<string, StatNameGenerated>;

export type UserModel = {
  prisma: PrismaClient,
  uid: string,
  email?: string,
  name?: string
}

export type PokemonModel = {
  prisma: PrismaClient,
  uid: string,
  pokemonId?: number,
  name?: string,
  id?: string
}