import { PokemonModel } from "@utils/types";

export const Pokemon = ({ prisma, uid, pokemonId, name }: PokemonModel) => {
  const getPokemonByUser = async () => await prisma.pokemon.findFirst({ where: { pokemonId, userId: uid } });

  const createPokemon = async () => await prisma.pokemon.create({ data: { name, pokemonId, user: { connect: { id: uid } } } });

  const getAllPokemonsByUser = async () => await prisma.pokemon.findMany({ where: { userId: uid } });

  const removePokemonByUser = async ({ id }: { id: string }) => await prisma.pokemon.delete({ where: { id } })

  return {
    getPokemonByUser,
    createPokemon,
    getAllPokemonsByUser,
    removePokemonByUser
  }
}