import { Pokemon } from "@generated/graphql";
import { MapStatName } from "@utils/constants";
import { PokemonListResponse, PokemonResponse } from "@utils/types";
import { RESTDataSource } from "apollo-datasource-rest";
class PokemonAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "https://pokeapi.co/api/v2/";
  }

  async listPokemons(offset: number): Promise<PokemonListResponse> {
    try {
      return await this.get<PokemonListResponse>(`pokemon?limit=50&offset=${offset}`);
    } catch (error) {
      throw new Error('Could not get pokemons: ' + error);
    }
  }

  async getPokemonDetail(name: string): Promise<Partial<Pokemon>> {
    try {
      const { sprites: { front_default }, stats, ...rest } = await this.get<PokemonResponse>(`pokemon/${name}`);
      const pokemonStats = stats.map(stat => ({ name: MapStatName[stat.stat.name], value: stat.base_stat }))
      return {
        ...rest,
        stats: pokemonStats,
        image: front_default,
      }

    } catch (error) {
      throw new Error('Could not get pokemon detail ' + error);
    }
  }
}

export default PokemonAPI;
