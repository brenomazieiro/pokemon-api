import { PrismaClient } from "@prisma/client";
import { validateIdToken } from "@services/firebase/auth";
import { Context } from "@utils/types";
import { AuthenticationError } from "apollo-server";
import { ExpressContext } from "apollo-server-express/dist/ApolloServer";

const prisma = new PrismaClient()

export const createContext = async ({ req }: ExpressContext): Promise<Omit<Context, "dataSources">> => {
  const authorization = req.headers.authorization;

  if (!authorization) throw new AuthenticationError("User not autheticated");

  const idToken = authorization.split(" ")[1];

  const validatedIdToken = await validateIdToken(idToken);

  if (!validatedIdToken.uid) throw new AuthenticationError("Token expired");

  return {
    uid: validatedIdToken.uid,
    prisma
  }
}