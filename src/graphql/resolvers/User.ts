import { UserResolvers } from "@generated/graphql";
import { Pokemon } from "@models/Pokemon";
import { Context } from "@utils/types";

const User: UserResolvers = {
  pokemons: async (_, __, { uid, prisma, dataSources }: Context) => {
    const pokemonModel = Pokemon({ uid, prisma });
    const pokemonList = await pokemonModel.getAllPokemonsByUser();
    const pokemons = await Promise.all(pokemonList.map(pokemon => dataSources.pokemonAPI.getPokemonDetail(pokemon.name)));
    return pokemons;
  }
}

export default User;