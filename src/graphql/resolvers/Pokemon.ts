import { PokemonResolvers } from "@generated/graphql";
import { Pokemon as PokemonModel, User } from "@models/index";
import { Context } from "@utils/types";

const Pokemon: PokemonResolvers = {
  user: async (_, __, { uid, prisma }: Context) => {
    const userModel = User({ uid, prisma });
    return await userModel.getUser();
  },
  wasCatched: async (pokemon, __, { uid, prisma }: Context) => {
    const pokemonModel = PokemonModel({ uid, prisma, pokemonId: pokemon.id });
    const wasCatched = await pokemonModel.getPokemonByUser();
    return !!wasCatched;
  }
}

export default Pokemon;