import "graphql-import-node";
import resolvers from "./resolvers";
import * as typeDefs from "./schema.graphql";

const schema = {
  typeDefs,
  resolvers
};

export default schema;