import app from "./index";

export const validateIdToken = async (idToken: string): Promise<{ uid?: string }> => {
  try {
    return await app.auth().verifyIdToken(idToken).then(decodedToken => ({ uid: decodedToken.uid }));
  } catch {
    return { uid: undefined };
  }
}